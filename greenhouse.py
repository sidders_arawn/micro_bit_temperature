from microbit import *
# import os

# temperature variable

currentTemp = temperature()

# The loop. Temperature logged every 2 mins and the result written to a file.
# As you cannot append to a file in Micropython, the content of the file
# is copied to a variable and written back to the file along with the new reading.

while True:
    # open file and read, hold them in variable 'content'.
    try:
        with open('inlog.txt') as f:
            content = f.read()

    # If log.txt doesn't exist. Create it and write 1st entry.
    except OSError:
        with open('inlog.txt', 'w') as a:
            a.write("%s" % (currentTemp))

    # Open the file to write 'content' back to it plus the new entry 'currentTemp'.

    else:
        with open('inlog.txt', 'w') as a:
            a.write("%s, %s" % (content, currentTemp))

            # If you want a sneaky peak at the currentTemp,  press button a.
        while True:
            if button_a.was_pressed():
                display.scroll(temperature())
        break
# sleep 1 hour, providing this is in Mili seconds
sleep(360)


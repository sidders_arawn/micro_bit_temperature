import matplotlib.pyplot as plt

input ("Ensure the log file; inlog.txt and outlog.txt are iun the same folder as this program. Press Enter to continue")

#Create lists for Y axis
intemp_y1=[]
outtemp_y2=[]

#Extract data fro the logs and append to the lists for Y

with open ('inlog.txt', 'r') as f:
     intemp=f.read()
intemp_y1=intemp.split(',')

with open ('outlog.txt','r') as f:
     outtemp=f.read()
outtemp_y2=outtemp.split(',')

#Find lengh of time the temp has been logged (number of hours) X axis

hours_x=len(intemp_y1)

    
#Line graph or histogram, call the appropriate function
#graph_type=input ("which graph do you want to see a Line graph or Histogram?.Type L <Enter> for Line and H <Enter> for Histogram " )

#Make sure we're lower case
#graph_type=graph_type.lower

#if graph_type == "l":
#    Line_graph()
#else:
#    histogram()


#Line_graph
plt.plot(range(0,hours_x),intemp_y1)
plt.plot(range(0,hours_x),outtemp_y2)
plt.title("Greenhouse Temp")
plt.xlabel("Time\Hours")
plt.ylabel("Temperature c")
plt.show()
plt.show()

